# Almundo examen Frontend

_El exámen está compuesto por dos aplicaciones, un backend escrito en NodeJS el cual expondrá una API REST para la interacción con la aplicación frontend.
Adjunto a este documento encontrarás recursos que facilitarán el exámen, para que te enfoques en lo realmente importante._

## Ejercicio 1: API Rest NodeJS

_Para comenzar tenemos que desarrollar el API REST en NodeJS.
Este se encuentra en la carpeta APINode y para poderlo ejecutar hay que seguir los siguientes pasos_

### Pre-requisitos 

_1. Debemos tener instalado Node.js en nuestra computadora_

### Pasos a seguir 

_1. Ingresamos a la carpeta Node Js._
_2. Ejecutamos el siguiente comando para instalar las librerias necesarias para ejecutar el programa._

```
npm install
```

_3. Luego este comando para ejecutar nuestro codigo y crear el servidor Node._

```
node index.js
```

## Ejercicio 2: Frontend

_Maquetar una página de resultado de hoteles, ver imágenes en el repo (solo mobile y desktop).
Consumir la API desarrollada en el ejercicio anterior, implementando las funcionalidades necesarias para listar y filtar los hoteles._

_Para este caso usamos Angular 6_

### Pre-requisitos 

_1. Tener instalado el Angular CLI_

```
npm install -g @angular/cli
```

### Pasos a seguir 

_1. Ingresamos a la carpeta web-almundo_
_2. Ejecutamos el siguiente comando para instalar las librerias necesarias para ejecutar el programa._

```
npm install
```

_3. Luego este comando para ejecutar nuestra web._

```
ng serve --port 4200
```


_De esta manera podemos ejecutar el Back y el Front y ver el resultado_