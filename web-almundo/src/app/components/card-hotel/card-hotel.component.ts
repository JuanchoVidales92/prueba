import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'card-hotel',
    templateUrl: './card-hotel.component.html',
    styleUrls: []
})
export class CardHotelComponent implements OnInit {
    @Input() data: any;
    public stars = [];

    constructor() { }

    ngOnInit(): void {
        for (let i = 0; i < this.data.stars; i++) {
            this.stars.push(i);
        }
    }
}
