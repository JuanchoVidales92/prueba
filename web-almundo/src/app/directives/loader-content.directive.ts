import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
	  selector : '[loaderContent]'
})

export class LoaderContentDirective {
    private loaderHTML: string;
    @Input() loaderID: string;

    constructor (
        private el: ElementRef
    ) { }

    ngAfterViewInit() {
        this.loaderHTML = '<div id="' + this.loaderID + '" class="preloader-content"><div class="preloader"><img src="./assets/images/loader.gif"></div></div>';
        this.el.nativeElement.insertAdjacentHTML('beforeend', this.loaderHTML);
    }
}
