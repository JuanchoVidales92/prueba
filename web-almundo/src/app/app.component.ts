import { Component, OnInit } from '@angular/core';
import { CustomHttpService } from '../services/customHttp.service';
declare var $;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    public hotels: any;
    public stars = [5, 4, 3, 2, 1];
    public starsImages = [1, 2, 3, 4, 5];
    public filterName = '';
    public filterStar: any;
    public showFilterName = false;
    public showFilterStars = false;
    public showFilterAll = true;

    constructor (
        private customHttpService: CustomHttpService
    ) {}

    ngOnInit(): void {
        this.getAllHotels();
    }

    private getAllHotels(): void {
        const self = this;
        $('.preloader-content').fadeIn();
        self.customHttpService.httpGET (true, true, 'getAllHotels').then(
            function (response) {
                self.hotels = response['data'];
            }
        );
    }

    public showFilter(name: string): void {
        switch (name) {
            case 'name':
                this.showFilterName = !this.showFilterName;
            break;
            case 'stars':
                this.showFilterStars = !this.showFilterStars;
            break;
            case 'all':
                if (this.showFilterAll) {
                    $('.filter-name').show();
                    $('.filter-stars').show();
                } else {
                    $('.filter-name').hide();
                    $('.filter-stars').hide();
                }
                this.showFilterAll = !this.showFilterAll;
            break;
        }
    }

    public searchName() { this.searchHotel('name', this.filterName); }

    public searchStars($event) {
        $(".checkbox-star").prop("checked", false);
        $event.target.checked = true;
        this.searchHotel('stars', parseInt($event.target.value));
    }

    public searchHotel(typeFilter: string, value: any): void {
        const self = this;
        $('.preloader-content').fadeIn();
        const req = {
            'typeFilter': typeFilter,
            'filter': value
        }
        self.customHttpService.httpPOST (true, true, 'postFilterHotels', req).then(
            function (response) {
                self.hotels = response['data'];
            }
        );
    }
}
