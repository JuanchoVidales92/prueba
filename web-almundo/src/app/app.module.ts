import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

// Import Components
import { AppComponent } from './app.component';
import { CardHotelComponent } from './components/card-hotel/card-hotel.component';
import { StarComponent } from './components/star/star-svg.component';

// Import Directives
import { LoaderContentDirective } from './directives/loader-content.directive';

// Import Services
import { CustomHttpService } from '../services/customHttp.service';
import { UtilsService } from '../services/utils.service';

@NgModule({
  declarations: [
      AppComponent,
      CardHotelComponent,
      StarComponent,
      // Directives
      LoaderContentDirective
  ],
  imports: [
      BrowserModule,
      FormsModule,
      HttpClientModule
  ],
  providers: [
      UtilsService,
      CustomHttpService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
