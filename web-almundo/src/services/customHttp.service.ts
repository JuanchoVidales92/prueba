import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilsService } from './utils.service';

// Declares
declare var jquery: any;
declare var $: any;

@Injectable()
export class CustomHttpService {

    private url = 'http://localhost:3000/';
    private headerBaseFormData = new HttpHeaders();
    private headerBaseJson = new HttpHeaders().set('Content-Type', 'application/json');

    constructor(
        private http: HttpClient,
        private utilsService: UtilsService
    ) {}

    /**
     * Metodos de conexion con el API (GET, POST, PUT, GET-DATA)
     */
    // Metodo GET
    httpGET (tempLoader, msg, path): Promise<any> {
        return new Promise(
            resolve => {
                // Make the HTTP request:
                this.http.get(this.url + path, {
                    headers: this.headerBaseJson,
                }).subscribe(
                    // Successful responses call the first callback.
                    data => {
                        if (data != null && data.hasOwnProperty('errorCode')) {
                            if (msg) { this.utilsService.showError(data['error']); }
                            resolve(false);
                        } else {
                            if (tempLoader) { $('.preloader-content').fadeOut(); }
                            resolve(data);
                        }
                    },
                    // Errors will call this callback instead:
                    err => {
                        if (tempLoader) { $('.preloader-content').fadeOut(); }
                        if (msg) { this.utilsService.showError(err); }
                        resolve(false);
                    }
                );
            }
        );
    }

    // Metodo POST
    httpPOST (tempLoader, msg, path, params = null, formData = null): Promise<any> {
        return new Promise(
            resolve => {
                let varHeader = this.headerBaseJson;
                let varData = params;
                if (formData !== null) {
                    varHeader = this.headerBaseFormData;
                    varData = formData;
                }
                // Make the HTTP request:
                this.http.post(
                    this.url + path,
                    varData,
                    { headers: varHeader }
                ).subscribe(
                    // Successful responses call the first callback.
                    data => {
                        if (data != null && data.hasOwnProperty('errorCode')) {
                            if (msg) { this.utilsService.showError(data); }
                            resolve(false);
                        } else {
                            if (tempLoader) { $('.preloader-content').fadeOut(); }
                            resolve(data);
                        }
                    },
                    // Errors will call this callback instead:
                    err => {
                        if (tempLoader) { $('.preloader-content').fadeOut(); }
                        if (msg) { this.utilsService.showError(err); }
                        resolve(false);
                    }
                );
            }
        );
    }

    // Metodo POST
    httpPUT (tempLoader, msg, path, params = null, formData = null): Promise<any> {
        return new Promise(
            resolve => {
                let varHeader = this.headerBaseJson;
                let varData = params;
                if (formData !== null) {
                    varHeader = this.headerBaseFormData;
                    varData = formData;
                }
                // Make the HTTP request:
                this.http.put(
                    this.url + path,
                    varData,
                    { headers: varHeader }
                ).subscribe(
                    // Successful responses call the first callback.
                    data => {
                        if (data != null && data.hasOwnProperty('errorCode')) {
                            if (msg) { this.utilsService.showError(data['error']); }
                            resolve(false);
                        } else {
                            if (tempLoader) { $('.preloader-content').fadeOut(); }
                            resolve(data);
                        }
                    },
                    // Errors will call this callback instead:
                    err => {
                        if (tempLoader) { $('.preloader-content').fadeOut(); }
                        if (msg) { this.utilsService.showError(err); }
                        resolve(false);
                    }
                );
            }
        );
    }
    /**
     * Fin Metodos de conexion con el API (GET, POST, PUT, GET-DATA)
     */

}
