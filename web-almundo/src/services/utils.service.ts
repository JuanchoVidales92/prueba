import { Injectable } from '@angular/core';

@Injectable()
export class UtilsService {

    private isDebug = true;

    constructor() { }

    debugConsole (message) {
        if (this.isDebug === true) {
            console.log(message);
        }
    }

    alert (message) {
        alert(message);
    }

    showError (message) {
        if (message.hasOwnProperty('success')) {
            this.debugConsole(message);
            alert(message.error);
        } else {
            if (message.status === 401) {
            } else if (message.status === 400) {
                this.debugConsole(message['error'].error);
                alert(message['error'].error);
            } else if (message.status === 404) {
                this.debugConsole(message);
                if (message.error === null) {
                    alert('La Url consultada no responde');
                } else {
                    alert(message['error'].error);
                }
            } else if (message.status === 503 || message.status === 500 || message.status === 0) {
                if (message.hasOwnProperty('message')) {
                    this.alert(message.message);
                }
            } else if (this.isDebug === true) {
                this.debugConsole(message);
                alert(message.message);
            }
        }
    }
}
